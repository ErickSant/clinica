import java.util.HashMap;

public class UnidadMedica {

    private int idUnidad;
    private String nombre;
    private int planta;
    private Doctor doctor;
    private HashMap<Integer, Paciente> dataPaciente;

    public UnidadMedica(int idUnidad, String nombre, int planta, Doctor doctor,
            HashMap<Integer, Paciente> dataPaciente) {
        this.idUnidad = idUnidad;
        this.nombre = nombre;
        this.planta = planta;
        this.doctor = doctor;
        this.dataPaciente = dataPaciente;

    }

    public int getIdUnidad() {
        return idUnidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPlanta() {
        return planta;
    }

    public void setPlanta(int planta) {
        this.planta = planta;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public HashMap<Integer, Paciente> getDataPaciente() {
        return this.dataPaciente;
    }

    public void setDataPaciente(HashMap<Integer, Paciente> dataPaciente) {
        this.dataPaciente = dataPaciente;
    }

}
