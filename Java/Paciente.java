import java.util.HashMap;

public class Paciente {
    private int IdPaciente;
    private String nombre;
    private int edad;
    private String telefono;
    private String unidadIngreso;
    private boolean alta;
    private HashMap <Integer, Paciente> dataPaciente;

    public Paciente(int IdPaciente, String nombre, int edad, String telefono, String unidadIngreso, boolean alta) {
        this.IdPaciente = IdPaciente;
        this.nombre = nombre;
        this.edad = edad;
        this.telefono = telefono;
        this.unidadIngreso = unidadIngreso;
        this.alta = alta;
    }
    public int getIdPaciente() {
        return IdPaciente;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public int getEdad() {
        return edad;
    }
    public void setEdad(int edad) {
        this.edad = edad;
    }
    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public String getUnidadIngreso() {
        return unidadIngreso;
    }
    public void setUnidadIngreso(String unidadIngreso) {
        this.unidadIngreso = unidadIngreso;
    }
    public boolean getAlta() {
        return alta;
    }
    public void setAlta(boolean alta) {
        this.alta = alta;
    }
    public void alta() {
        setAlta(true);
    }
    public void entraClinica() {
        setAlta(false);
    }
    public HashMap<Integer, Paciente> getDataPaciente() {
        return dataPaciente;
    }
    public void setDataPaciente(HashMap<Integer, Paciente> dataPaciente) {
        this.dataPaciente = dataPaciente;
    }
}