public class Doctor {
    private int idDoctor;
    private String nombre;
    private int edad;
    private String telefono;

    public Doctor(int idDoctor, String nombre, int edad , String telefono){
        this.idDoctor = idDoctor;
        this.nombre=nombre;
        this.edad=edad;
        this.telefono=telefono;
    }

    public int getIdDoctor() {
        return idDoctor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}