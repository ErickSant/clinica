import java.util.HashMap;
import java.util.Scanner;

public class Menu {
    public void menuClinica(HashMap<Integer, Paciente> dataPaciente, HashMap<Integer, UnidadMedica> dataUnidad,
            HashMap<Integer, Doctor> dataDoctor) {
        int opcion;
        do {
            Scanner sc = new Scanner(System.in);
            System.out.println("---- Bienvenido al menu. ----");
            System.out.println("Seleccione una opcion.");
            System.out.println(
                    "\n1. Registrar paciente. \n2. Registrar doctor. \n3. Registrar nueva unidad medica. \n4. Salir.");
            opcion = sc.nextInt();
            switch (opcion) {
                case 1:
                    do {
                        System.out.println("--- Pacientes ---");
                        System.out.println("Seleccione una opcion.");
                        System.out.println(
                                "\n1. Alta paciente. \n2. Baja paciente. \n3. Modificar paciente. \n4. Consultar paciente. \5. Salir.");
                        opcion = sc.nextInt();
                        switch (opcion) {
                            case 1:
                                System.out.println("--- Alta paciente ---");
                                System.out.println("Ingrese los datos del paciente:");
                                System.out.println("Nombre: ");
                                String nombre = sc.next();
                                System.out.println("Edad: ");
                                int edad = sc.nextInt();
                                System.out.println("Telefono: ");
                                String telefono = sc.next();
                                System.out.println("Unidad a ingresar: ");
                                // Imprimir lista unidades
                                System.out.println("Seleccione unidad medica.");
                                dataUnidad.forEach((key, value) -> System.out.println(key + " - " + value.getNombre()));
                                opcion = sc.nextInt();
                                String unidadIngreso = dataUnidad.get(opcion).getNombre();
                                int id = (dataPaciente.size() + 1);
                                Paciente pacienteBase = new Paciente(id, nombre, edad, telefono, unidadIngreso, false);
                                dataPaciente.put(pacienteBase.getIdPaciente(), pacienteBase);
                                System.out.println("Paciente dado de alta con exito.");
                                break;
                            case 2:
                                System.out.println("--- Baja paciente ---");
                                System.out.println("Seleccione el paciente a eliminar.");
                                dataPaciente
                                        .forEach((key, value) -> System.out.println(key + " - " + value.getNombre()));
                                opcion = sc.nextInt();
                                dataPaciente.remove(opcion);
                                System.out.println("Paciente dado de baja con exito.");
                                break;
                            case 3:
                                System.out.println("--- Modificar paciente ---");
                                System.out.println("Seleccione paciente a modificar informacion: ");
                                dataPaciente
                                        .forEach((key, value) -> System.out.println(key + " - " + value.getNombre()));
                                opcion = sc.nextInt();
                                System.out.println("Informacion del paciente: ");
                                System.out.println("1- Nombre: " + dataPaciente.get(opcion).getNombre());
                                System.out.println("2- Edad: " + dataPaciente.get(opcion).getEdad());
                                System.out.println("3- Telefono: " + dataPaciente.get(opcion).getTelefono());
                                System.out.println(
                                        "4- Unidad de ingreso: " + dataPaciente.get(opcion).getUnidadIngreso());
                                System.out.println("5- Alta: " + dataPaciente.get(opcion).getAlta());
                                System.out.println("¿Que informacion desea modificar?");
                                opcion = sc.nextInt();
                                switch (opcion) {
                                    case 1:
                                        System.out.println("Nuevo nombre: ");
                                        String nuevoNombre = sc.next();
                                        dataPaciente.get(opcion).setNombre(nuevoNombre);
                                        System.out.println("Informacion actualizada.");
                                        break;
                                    case 2:
                                        System.out.println("Nueva edad: ");
                                        int nuevaEdad = sc.nextInt();
                                        dataPaciente.get(opcion).setEdad(nuevaEdad);
                                        System.out.println("Informacion actualizada.");
                                        break;
                                    case 3:
                                        System.out.println("Nuevo telefono: ");
                                        String nuevoTelefono = sc.next();
                                        dataPaciente.get(opcion).setTelefono(nuevoTelefono);
                                        System.out.println("Informacion actualizada.");
                                        break;
                                    case 4:
                                        System.out.println("Nueva unidad medica: ");
                                        String nuevaUnidad = sc.next();
                                        dataPaciente.get(opcion).setUnidadIngreso(nuevaUnidad);
                                        ;
                                        System.out.println("Informacion actualizada.");
                                        break;
                                    case 5:
                                        System.out.println("Alta: ");
                                        dataPaciente.get(opcion).setAlta(true);
                                        System.out.println("Informacion actualizada.");
                                        break;
                                }
                                break;
                            case 4:
                                System.out.println("--- Consultar paciente ---");
                                System.out.println("Seleccione paciente a consultar.");
                                dataPaciente
                                        .forEach((key, value) -> System.out.println(key + " - " + value.getNombre()));
                                opcion = sc.nextInt();
                                System.out.println("Informacion del paciente: ");
                                System.out.println("Id: " + dataPaciente.get(opcion).getIdPaciente());
                                System.out.println("Nombre: " + dataPaciente.get(opcion).getNombre());
                                System.out.println("Edad: " + dataPaciente.get(opcion).getEdad());
                                System.out.println("Telefono: " + dataPaciente.get(opcion).getTelefono());
                                System.out.println("Unidad de ingreso: " + dataPaciente.get(opcion).getUnidadIngreso());
                                System.out.println("Alta: " + dataPaciente.get(opcion).getAlta());
                        }
                    } while (opcion != 5);
                    break;

                case 2:
                    do {
                        System.out.println("--- Doctor ---");
                        System.out.println("Seleccione una opcion.");
                        System.out.println(
                                "\n1. Alta Doctor. \n2. Baja Doctor. \n3. Modificar Doctor. \n4. Consultar Doctor. \5. Salir.");
                        opcion = sc.nextInt();
                        switch (opcion) {
                            case 1:
                                System.out.println("--- Alta Doctor ---");
                                System.out.println("Ingrese los datos del Doctor:");
                                System.out.println("Nombre: ");
                                String nombre = sc.next();
                                System.out.println("Edad: ");
                                int edad = sc.nextInt();
                                System.out.println("Telefono: ");
                                String telefono = sc.next();
                                System.out.println("Unidad a ingresar: ");

                                int id = (dataDoctor.size() + 1);
                                Doctor DoctorBase = new Doctor(id, nombre, edad, telefono, unidadIngreso, false);
                                dataDoctor.put(DoctorBase.getIdDoctor(), DoctorBase);
                                System.out.println("Doctor dado de alta con exito.");
                                break;
                            case 2:
                                System.out.println("--- Baja Doctor ---");
                                System.out.println("Seleccione el Doctor a eliminar.");
                                dataDoctor.forEach((key, value) -> System.out.println(key + " - " + value.getNombre()));
                                opcion = sc.nextInt();
                                dataDoctor.remove(opcion);
                                System.out.println("Paciente dado de baja con exito.");
                                break;
                            case 3:
                                System.out.println("--- Modificar Doctor ---");
                                System.out.println("Seleccione paciente a modificar informacion: ");
                                dataDoctor.forEach((key, value) -> System.out.println(key + " - " + value.getNombre()));
                                opcion = sc.nextInt();
                                System.out.println("Informacion del paciente: ");
                                System.out.println("1- Nombre: " + dataDoctor.get(opcion).getNombre());
                                System.out.println("2- Edad: " + dataDoctor.get(opcion).getEdad());
                                System.out.println("3- Telefono: " + dataDoctor.get(opcion).getTelefono());
                                System.out
                                        .println("4- Unidad de ingreso: " + dataDoctor.get(opcion).getUnidadIngreso());
                                System.out.println("5- Alta: " + dataDoctor.get(opcion).getAlta());
                                System.out.println("¿Que informacion desea modificar?");
                                opcion = sc.nextInt();
                                switch (opcion) {
                                    case 1:
                                        System.out.println("Nuevo nombre: ");
                                        String nuevoNombre = sc.next();
                                        dataDoctor.get(opcion).setNombre(nuevoNombre);
                                        System.out.println("Informacion actualizada.");
                                        break;
                                    case 2:
                                        System.out.println("Nueva edad: ");
                                        int nuevaEdad = sc.nextInt();
                                        dataDoctor.get(opcion).setEdad(nuevaEdad);
                                        System.out.println("Informacion actualizada.");
                                        break;
                                    case 3:
                                        System.out.println("Nuevo telefono: ");
                                        String nuevoTelefono = sc.next();
                                        dataDoctor.get(opcion).setTelefono(nuevoTelefono);
                                        System.out.println("Informacion actualizada.");
                                        break;
                                    case 4:
                                        System.out.println("Nueva unidad medica: ");
                                        String nuevaUnidad = sc.next();
                                        dataDoctor.get(opcion).setUnidadIngreso(nuevaUnidad);
                                        ;
                                        System.out.println("Informacion actualizada.");
                                        break;
                                    case 5:
                                        System.out.println("Alta: ");
                                        dataDoctor.get(opcion).setAlta(true);
                                        System.out.println("Informacion actualizada.");
                                        break;
                                }
                                break;
                            case 4:
                                System.out.println("--- Consultar Doctor ---");
                                System.out.println("Seleccione Doctor a consultar.");
                                dataDoctor.forEach((key, value) -> System.out.println(key + " - " + value.getNombre()));
                                opcion = sc.nextInt();
                                System.out.println("Informacion del Doctor: ");
                                System.out.println("Id: " + dataDoctor.get(opcion).getIdPaciente());
                                System.out.println("Nombre: " + dataDoctor.get(opcion).getNombre());
                                System.out.println("Edad: " + dataDoctor.get(opcion).getEdad());
                                System.out.println("Telefono: " + dataDoctor.get(opcion).getTelefono());
                                System.out.println("Unidad de ingreso: " + dataDoctor.get(opcion).getUnidadIngreso());
                                System.out.println("Alta: " + dataDoctor.get(opcion).getAlta());
                        }
                        break;
                    } while (opcion != 4);

                case 3:
                    do {
                        System.out.println("--- Unidad medica ---\nSeleccione una opcion.");
                        System.out.println(
                                "\n1. Alta unidad medica.\n2. Baja unidad medica.\n3. Modificar unidad medica.\n4. Consultar unidad medica.\n5. Salir.");
                        opcion = sc.nextInt();
                        switch (opcion) {
                            case 1:
                                System.out.println(
                                        "--- Alta unidad medica ---\nIngrese los datos de la unidad medica...");
                                System.out.println("Nombre: ");
                                String nombre = sc.next();
                                System.out.println("Planta: ");
                                int Planta = sc.nextInt();
                                System.out.println("Selecciona el doctor encargado de la unidad");
                                dataDoctor.forEach((key, value) -> System.out.println(key + " - " + value.getNombre()));
                                opcion = sc.nextInt();
                                Doctor doctor = dataDoctor.get(opcion);
                                int id = (dataUnidad.size() + 1);
                                UnidadMedica plantillaUnidad = new UnidadMedica(opcion, nombre, Planta, doctor,
                                        dataPaciente);
                                dataUnidad.put(id, plantillaUnidad);
                                System.out.println("Unidad medica dada de alta de manera correcta");
                                break;
                            case 2:
                                System.out.println("--- Baja Unidad medica ---\nSeleccione la unidad a eliminar.");
                                dataUnidad.forEach((key, value) -> System.out.println(key + " - " + value.getNombre()));
                                opcion = sc.nextInt();
                                dataPaciente.remove(opcion);
                                System.out.println("Unidad medica dada de baja de manera correcta");
                                break;
                            case 3:
                                System.out.println("--- Modificar unidad medica ---");
                                System.out.println("Seleccione unidad medica que desea modificar: ");
                                dataUnidad.forEach((key, value) -> System.out.println(key + " - " + value.getNombre()));
                                opcion = sc.nextInt();
                                System.out.println("Informacion de la unidad medica: ");
                                System.out.println("1- Nombre: " + dataUnidad.get(opcion).getNombre());
                                System.out.println("2- Planta: " + dataUnidad.get(opcion).getPlanta());
                                System.out.println("3- Doctor: " + dataUnidad.get(opcion).getDoctor());
                                System.out.println("4- Pacientes: " + dataUnidad.get(opcion).getDataPaciente());
                                System.out.println("¿Que desea modificar?");
                                int opcionUnidad = sc.nextInt();
                                switch (opcionUnidad) {
                                    case 1:
                                        System.out.println("Nuevo nombre: ");
                                        String nuevoNombre = sc.next();
                                        dataUnidad.get(opcion).setNombre(nuevoNombre);
                                        System.out.println("Informacion actualizada.");
                                        break;
                                    case 2:
                                        System.out.println("Nueva planta: ");
                                        int nuevaPlanta = sc.nextInt();
                                        dataUnidad.get(opcion).setPlanta(nuevaPlanta);
                                        System.out.println("Informacion actualizada.");
                                        break;
                                    case 3:
                                        System.out.println("Nuevo Doctor: ");
                                        dataDoctor.forEach(
                                                (key, value) -> System.out.println(key + " - " + value.getNombre()));
                                        opcion = sc.nextInt();
                                        Doctor doctorNuevo = dataDoctor.get(opcion);
                                        dataUnidad.get(opcionUnidad).setDoctor(doctorNuevo);
                                        System.out.println("Infomacion actualizada");
                                        break;
                                    case 4:
                                        System.out.println(
                                                "Nuevo paciente:\nSeleciona el paciente que quieres modificar: ");
                                        dataPaciente.forEach(
                                                (key, value) -> System.out.println(key + " - " + value.getNombre()));
                                        opcion = sc.nextInt();
                                        System.out.println("Ingresa el nuevo paciente: ");
                                        Paciente paciente;
                                        dataPaciente.put(opcion, paciente);
                                        System.out.println("Informacion actualizada");
                                        break;
                                }
                                break;
                            case 4:
                                System.out.println("--- Consultar Unidad medica ---");
                                System.out.println("Seleccione unidad a consultar.");
                                dataUnidad.forEach((key, value) -> System.out.println(key + " - " + value.getNombre()));
                                opcion = sc.nextInt();
                                System.out.println("Informacion de la unidad medica ");
                                System.out.println("Id: " + dataUnidad.get(opcion).getIdUnidad());
                                System.out.println("1- Nombre: " + dataUnidad.get(opcion).getNombre());
                                System.out.println("2- Planta: " + dataUnidad.get(opcion).getPlanta());
                                System.out.println("3- Doctor: " + dataUnidad.get(opcion).getDoctor());
                                System.out.println("4- Pacientes: " + dataUnidad.get(opcion).getDataPaciente());
                        }
                    } while (opcion != 5);
                    break;

            }
        } while (opcion != 4);
    }
}